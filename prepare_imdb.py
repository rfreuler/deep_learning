# -*- coding: utf-8 -*-
"""
Created on 02 Apr 02.04.17 11:04 2017 

@author: benzi

Description:

prepares the sql export from imdb movie database... it ha 2000 forum posts per category (1-10)

- creates a folder for each character
- creates a .txt file with the line in the folder of each character

"""
import pandas as pd
import os
import shutil

def mkdir(path):
    if not os.path.isdir(path):
        os.makedirs(path)

DATA = "data/imdb/"
ROOT = "data/imdb/categories"
# DATA = "data/imdb_test/"
# ROOT = "data/imdb_test/categories"

# remove existing datafolder and create new one
if os.path.isdir(ROOT):
    shutil.rmtree(ROOT)
    mkdir(ROOT)
else:
    mkdir(ROOT)

# GET Top 10 Characters from csv file
raw_datas = []
for i in range(1, 11):
    file = DATA + str(i) + ".csv"
    print("read", file)
    raw_datas.append(pd.read_csv(file, delimiter=';;', header=None))

# prepare data for each character
for index, raw_data in enumerate(raw_datas):
    path = ROOT + "/" + str(index)
    mkdir(path)

    #lines = raw_data.loc[raw_data['Character'].isin(['Cartman'])]
    for filenr,line in raw_data.iterrows():
        if len(line[0]) > 0:
            filename = path + "/" + str(filenr) + ".txt"
            with open(filename, 'w') as f:
                f.write(str(line[0]))