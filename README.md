**[This code belongs to the "Implementing a CNN for Text Classification in Tensorflow" blog post.](http://www.wildml.com/2015/12/implementing-a-cnn-for-text-classification-in-tensorflow/)**

It is slightly simplified implementation of Kim's [Convolutional Neural Networks for Sentence Classification](http://arxiv.org/abs/1408.5882) paper in Tensorflow.

## Requirements

- Python 3
- Tensorflow > 0.12
- Numpy

## word2vec
Download word2vec here: 

https://drive.google.com/uc?id=0B7XkCwpI5KDYNlNUTTlSS21pQmM&export=download

put the GoogleNews-vectors-negative300.bin file to: 
```bash
data/word2vec/GoogleNews-vectors-negative300.bin 
```
_(or edit config.yml accordingly)_

## Southpark Dialog
Orig. Kaggle file is under: 

https://www.kaggle.com/tovarischsukhov/southparklines

Save under:
 
data/southpark/All-seasons.csv

run the helper script "./prepare_southpark.py" to get prepare the data for the CNN
```bash
./prepare_southpark.py
```

The script will analyze the top10 characters in the original csv with most dialoges. 
It will then prepare the data to run the Training on them with localdata dataset.

Format:
```bash
- data/southpark/character
 |_ 0
   |_0.txt
   |_1.txt
```|_2.txt
 |_ 1
   |_0.txt
   |_1.txt
 |_ 2
   |_0.txt
   |_1.txt
etc.
```

## IMDB Forum
data/imdb/*.csv

run the helper script "./prepare_imdb.py"

## Training

Print parameters:

```bash
./train.py --help
```

```
optional arguments:
  -h, --help            show this help message and exit
  --embedding_dim EMBEDDING_DIM
                        Dimensionality of character embedding (default: 128)
  --enable_word_embeddings
                        Enable/disable the word embeddings (default: True)
  --filter_sizes FILTER_SIZES
                        Comma-separated filter sizes (default: '3,4,5')
  --num_filters NUM_FILTERS
                        Number of filters per filter size (default: 128)
  --l2_reg_lambda L2_REG_LAMBDA
                        L2 regularizaion lambda (default: 0.0)
  --dropout_keep_prob DROPOUT_KEEP_PROB
                        Dropout keep probability (default: 0.5)
  --batch_size BATCH_SIZE
                        Batch Size (default: 64)
  --num_epochs NUM_EPOCHS
                        Number of training epochs (default: 100)
  --evaluate_every EVALUATE_EVERY
                        Evaluate model on dev set after this many steps
                        (default: 100)
  --checkpoint_every CHECKPOINT_EVERY
                        Save model after this many steps (default: 100)
  --allow_soft_placement ALLOW_SOFT_PLACEMENT
                        Allow device soft device placement
  --noallow_soft_placement
  --log_device_placement LOG_DEVICE_PLACEMENT
                        Log placement of ops on devices
  --nolog_device_placement

```

Train:

```bash
./train.py
```

## Evaluating

```bash
./eval.py --eval_train --checkpoint_dir="./runs/1459637919/checkpoints/"
```

Replace the checkpoint dir with the output from the training. To use your own data, change the `eval.py` script to load your data.


## References

- [Convolutional Neural Networks for Sentence Classification](http://arxiv.org/abs/1408.5882)
- [A Sensitivity Analysis of (and Practitioners' Guide to) Convolutional Neural Networks for Sentence Classification](http://arxiv.org/abs/1510.03820)