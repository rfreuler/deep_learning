# -*- coding: utf-8 -*-
"""
Created on 02 Apr 02.04.17 11:04 2017 

@author: trafrro

Description:

prepares the kaggle .csv file with more than 70k of southpark dialog into 
usable format.

- top 10 character
- creates a folder for each character
- creates a .txt file with the line in the folder of each character

"""
import pandas as pd
import numpy as np
import os
import shutil

def mkdir(path):
    """ create new folder
    """
    if not os.path.isdir(path):
        os.makedirs(path)

def cleanup(path):
    """ remove existing datafolder and create new one
    """
    if os.path.isdir(path):
        shutil.rmtree(path)
        mkdir(path)
    else:
        mkdir(path)

def calc_test(count, quote):
    """ returns the number of training / test set
    """
    train = int(count * quote)
    test = count - train
    return train, test


TOP = 10
DATA = "data/southpark/All-seasons.csv"
ROOT_Train = "data/southpark/character_train"
ROOT_Test = "data/southpark/character_test"
train_test = 0.8
max_diff = 1000 # define max difference in the amount of dialog lines between the characters


# Cleanup data structure
cleanup(ROOT_Train)
cleanup(ROOT_Test)

# GET Top X Characters from csv file
raw_data = pd.read_csv(DATA)
topX = raw_data.groupby(['Character']).size().sort_values(ascending=False).head(TOP)
topXchar = topX.index.values

# get smallest dataset
smallest = topX.to_frame().min().values[0]
max_lines = smallest + max_diff

# prepare data for each character
for index, character in enumerate(topXchar):
    print(index, character)
    train_path = ROOT_Train + "/" + str(index)
    test_path = ROOT_Test + "/" + str(index)
    mkdir(train_path)
    mkdir(test_path)
    lines = raw_data.loc[raw_data['Character'].isin([character])]
    trainc, testc = calc_test(len(lines), train_test)
    if trainc > max_lines:
        trainc = max_lines
    elif testc > max_lines:
        testc = max_lines
    print("split test:", testc, "train:", trainc)

    # shuffle lines
    lines = lines.reindex(np.random.permutation(lines.index))

    test_filenr = 0
    train_filenr = 0
    test_counter = 0
    train_counter = 0
    for line in lines.values:
        if len(line[3]) > 0:
            # write test set
            if test_counter <= testc:
                filename = test_path +"/"+ str(test_filenr) + ".txt"
                with open(filename, 'w') as f:
                    f.write(str(line[3]))
                test_filenr += 1
                test_counter += 1
            # write train set
            else:
                if train_counter <= max_lines:
                    filename = train_path + "/" + str(train_filenr) + ".txt"
                    with open(filename, 'w') as f:
                        f.write(str(line[3]))
                    train_filenr += 1
                    train_counter += 1
                else:
                    break